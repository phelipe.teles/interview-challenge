/*
  EXERCICIO DO MARKETPLACE
    - Dado uma lista de empréstimos e de investimentos, queremos gerar uma nova
      lista para exibir os empréstimos no marketplace.

    - Cada emprestimo é composto de (id, totalRequestedAmountCents, category,
      expiresAt)

    - Cada investimento (id, totalInvestedAmountCents, loanId)

    - Cada item do marketplace contem os seguintes dados (id,
      totalRequestedAmount, expiresAt, category, totalInvestedAmount)

    - A lista do marketplace deve ser ordenada por categoria e por data de
      expiração. Ou seja, digamos que tenhamos 2 emprestimos na msm categoria
      "X". Queremos priorizar o emprestimo que expirará em breve primeiro.

    - As categorias são (X, Y, Z), sendo que seguem a seguinte ordem de
      prioridade Z > X > Y
*/

import { Category, Investment, Loan, MarketplaceItem } from './types'

export const generateMarketplaceList = (
  loanList: Loan[],
  investmentList: Investment[]
): MarketplaceItem[] => {}
