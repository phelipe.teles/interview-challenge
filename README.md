# Mutual Interview Template

## Scripts

Below is a description of each included script

| Script | Description |
| ------ | ------ |
| test | Run jest to test your application  |
| test:watch | Run jest in watch-mode to test your application  |
